<html>
<h1>Forest Fire Statistic</h1>

<head>
<style>
body {
  background-color: lightblue;
}

h1 {
  color: black;
  font-weight: bold;    
  text-align: center;
  font-family: "Arial Black", Gadget, sans-serif;
  text-decoration: underline;
}
    
table, td {
  border: 1px solid black;
  margin-left:auto; 
  margin-right:auto;
  height: 50px;
  font-size: 90%;
}
    
p, b, a {
    text-align: center;    
    font-family: "Arial Black", Gadget, sans-serif;
    font-size: 15px;
}
    
footer {
   position: fixed;
   left: 0;
   bottom: 0;
   width: 100%;
   text-align: center;
   background-color: white;
   color: black;
}
    
a {
text-decoration: none;
color: black;
text-decoration: underline;
}

a:visited {
text-decoration: none;
color: black;
} 

</style>
    
</head>

<body>
    
    <p>Welcome to Forest Fire Statistic. This website was created to inform users about the devastating bush fires that occured in Portugal in 2007.</p>
    <p>The database that was prepared helped us in creating the queries and visually displaying them for our users to see.</p>

    
    <table style="width:40%">
        <tr>
            <td><a href="query1.php">Average area burned and relative humidity</a></td>
        </tr>
        <tr>
            <td><a href="query2.php">Average area burned and temperature</a></td>
        </tr>
        <tr>
            <td><a href="query3.php">Average area burned and wind speed</a></td>
        </tr>
        <tr>
            <td><a href="query4.php">Total area burned month</a></td>
        </tr>
        <tr>
            <td><a href="query5.php">Total area burned by day</a></td>
        </tr>
        <tr>
            <td><a href="query6.php">Total area burned and coordinates</a></td>
        </tr>
    </table>
    <p> Download database:<a href="http://www.dsi.uminho.pt/~pcortez/forestfires/forestfires.csv"> forestfires.csv</a> </p>
</body>

<footer>
<p>Created by Cherson Khoo - s3722279 & Rajneel Deo - s3723148</p>
</footer>
</html>
