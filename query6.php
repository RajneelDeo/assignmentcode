<html>
<h1><a href="https://assignment-277105.ts.r.appspot.com/">Forest Fire Statistic</a></h1>
<h1>Total area burned and coordinates</h1>

<head>
<style>
body {
  background-color: lightblue;
}

h1 {
  color: black;
  font-weight: bold;    
  text-align: center;
  font-family: "Arial Black", Gadget, sans-serif;
}
        
p {
    text-align: center;    
    font-family: "Arial Black", Gadget, sans-serif;
}
    
footer {
   left: 0;
   bottom: 0;
   width: 100%;
   text-align: center;
   background-color: white;
   color: black;
}
    
a {
text-decoration: none;
color: black;
text-decoration: underline;
}

a:visited {
text-decoration: none;
color: black;
} 

</style>
</head>

<body> 
    
<img src="https://storage.cloud.google.com/forestfirebucket/query6.PNG" alt="query1" width="900" height="500" style="vertical-align:middle;margin:0px 290px">
    
<p>The following graph shows the average area burned in hecatres based on coordinates (x,y)</p>    

<img src="https://storage.cloud.google.com/forestfirebucket/map.jpg" alt="map" width="900" height="500" style="vertical-align:middle;margin:0px 290px">
    
<p>The image above shows the Top 5 locations of most area burnd in hecatares and the coordinates of where the fire happened</p>

    
</body>
<br>
<footer>
<p style=" padding-bottom: 20px;">Created by Cherson Khoo - s3722279 & Rajneel Deo - s3723148</p>
</footer>

</html>